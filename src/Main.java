import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class Main {

    private static final int COUNT_THREAD = 3;
    private static final int COUNT = 10000;

    public static void main(String[] args) {
        task1();
        // task3();
    }

    public static void task1() {
        FutureTask<String> task1 = new FutureTask<String>(() -> {
            try {
                System.out.println("Run thread 1");
                Thread.sleep(5000);
                System.out.println("End thread 1");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, null);

        FutureTask<String> task2 = new FutureTask<String>(() -> {
            try {
                System.out.println("Run thread 2");
                Thread.sleep(2000);
                System.out.println("End thread 2");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, null);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        executor.submit(task1);
        executor.submit(task2);

        while (true) {
            try {
                // if both future task complete
                if (task1.isDone() && task2.isDone()) {

                    System.out.println("Both FutureTask Complete");

                    // shut down executor service
                    executor.shutdown();
                    return;
                }

                if (!task1.isDone()) {
                    System.out.println("Thread 1 still running.");
                }
                if (!task2.isDone()) {
                    System.out.println("Thread 2 still running.");
                }

                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println("Exception: " + e);
            }
        }
    }

    public static void task3() {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(COUNT_THREAD);
        List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < COUNT_THREAD; i++) {
            Thread thread = new Thread(() -> {

                try {
                    for (int j = 0; j < COUNT; j++) {
                        list.add(j);
                    }
                    System.out.println("Thread waitting.");
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println("All threads are done.");
            });
            thread.start();
        }
    }

}
