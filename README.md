# Zadání úkolů

1. V metodě main
    + Vytvořte dvě instance FutureTask<String>
        + První se uspí na 5s
        + Druhý se uspí na 2s
    + Vytvořte instanci ExecutorService 
        + Executors.newFixedThreadPool(2); 
    + Vytvořte nekonečný cyklus while
        + Ukončete program, pokud oba FutureTask dokončili svoji práci a vypíšete zprávu o ukončení
        + Vypíšete zprávu, pokud jedno z  FutureTask běží 
        + Uspěte vlákno na 1s 
2. Upravte náš chat(CV05), tak aby byl vláknově bezpečný
    + Vytvořte 3-4 vlákna a ozkoušejte
3. Bariéra - nepovinný
    + Zkuste naimplementovat

Cvičení 05:
https://gitlab.com/czmufakcz/ijav_cv05


